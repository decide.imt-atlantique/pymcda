FROM python:3.10.7

ARG USER_ID

RUN apt-get update && apt-get install -y git graphviz bash-completion pandoc

COPY requirements.txt /tmp/requirements.txt

RUN pip install -r /tmp/requirements.txt

RUN rm /tmp/requirements.txt

RUN adduser $USER_ID