#!/bin/bash

rm -rf tmp
mv tests tmp
git restore --source $1 tests
python -m pytest tests
STATUS=$?
rm -rf tests
mv tmp tests
exit $STATUS