.. PyMCDA documentation master file, created by
   sphinx-quickstart on Wed Aug  4 16:03:13 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=========================
Getting started on PyMCDA
=========================


.. toctree::
   :caption: Notebooks
   :hidden:

   examples
   tutorials

.. toctree::
   :caption: Documentation
   :hidden:

   api
   references
   changelog
   contributing

.. toctree::
   :caption: Links
   :hidden:

   PyPI <https://pypi.org/project/mcda/>
   Gitlab <https://gitlab.com/decide.imt-atlantique/pymcda>
   Issue Tracker <https://gitlab.com/decide.imt-atlantique/pymcda/-/issues>

This package is used as a basis to represent Multi-Criteria Decision Aiding (MCDA) problems as well as solve them.
It also contains relatively low-level plotting functions to visualize a MCDA problem and its solution.

It is released on PyPI as `mcda <https://pypi.org/project/mcda/>`_.

It is an open source project hosted on `gitlab <https://gitlab.com/decide.imt-atlantique/pymcda>`_.


Requirements
============

To be able to plot relations and outranking graphs, this package requires that `graphviz` be installed.
On Debian/Ubuntu this is done with:

.. code-block:: console

   $ sudo apt-get install graphviz


Installation
============

If you want to simply use this package, simply install it from `PyPI <https://pypi.org/project/mcda/>`_:

.. code-block:: console

   $ pip install mcda

.. note::

   This package can be installed with different optional dependencies to unlock full features:

   * plot: unlock plotting utilities
   * all: include all the above optional dependencies

   If you want all optional dependencies for instance, do:

   .. code-block:: console

      $ pip install "mcda[all]"

Once you installed our package, you can have a look at our :doc:`notebooks` section which contains examples
on how to use our package.

If you want to contribute to this package development, we recommend you to read the next section.


Contributors
============

This package is growing continuously and contributions are welcomed.
Contributions can come in the form of new features, bug fixes, documentation improvements
or any combination thereof.
It can also simply come in the form of issues describing the idea for a new feature, a bug encountered, etc.

If you want to contribute to this package, please read the :doc:`Contributing guidelines <contributing>`.
If you have any new ideas or have found bugs, feel free to `create an issue <https://gitlab.com/decide.imt-atlantique/pymcda/-/issues/new>`_.
Finally, any contribution must be proposed for integration as a `Merge Request <https://gitlab.com/decide.imt-atlantique/pymcda/-/merge_requests/new>`_.

Please visit our `Gitlab <https://gitlab.com/decide.imt-atlantique/pymcda>`_ for more details.

License
=======

This software is licensed under the `European Union Public Licence (EUPL) v1.2 <https://joinup.ec.europa.eu/page/eupl-text-11-12>`_.
For more information see `this <https://gitlab.com/decide.imt-atlantique/pymcda/-/blob/master/LICENSE>`_.


Citations
=========

.. todo:: write section


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
