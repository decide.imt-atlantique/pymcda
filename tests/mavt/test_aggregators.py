import unittest
from math import log
from typing import Any

from pandas import Series
from pandas.testing import assert_frame_equal, assert_series_equal

from mcda.functions import FuzzyNumber
from mcda.internal.core.aggregators import Aggregator
from mcda.matrices import (
    AdjacencyValueMatrix,
    PartialValueMatrix,
    PerformanceTable,
)
from mcda.mavt.aggregators import OWA, ULOWA, ChoquetIntegral, Sum, WeightedSum
from mcda.scales import FuzzyScale, NominalScale
from mcda.set_functions import Mobius, SetFunction
from mcda.values import CommensurableValues, Values


class AggregatorTestCase:
    """This class implements all tests common to Aggregator classes."""

    values: Values
    table: PerformanceTable
    partial_values: PartialValueMatrix
    aggregator: Aggregator
    aggregated_values: Any
    aggregated_table: CommensurableValues
    aggregated_partial_values: AdjacencyValueMatrix

    def test_within_in_scales(self):
        self.assertTrue(self.aggregator.within_in_scales(self.values.data))
        self.assertTrue(self.aggregator.within_in_scales(self.values))
        self.assertTrue(self.aggregator.within_in_scales(self.table.data))
        self.assertTrue(self.aggregator.within_in_scales(self.table))
        self.assertTrue(self.aggregator.within_in_scales(self.partial_values))

    def test_aggregate_series(self):
        self.assertEqual(
            self.aggregator(self.values.data), self.aggregated_values
        )

    def test_aggregate_values(self):
        self.assertEqual(
            self.aggregator(self.values),
            self.aggregated_values,
        )

    def test_aggregate_dataframe(self):
        assert_series_equal(
            self.aggregator(self.table.data), self.aggregated_table.data
        )

    def test_aggregate_value_matrix(self):
        self.assertEqual(self.aggregator(self.table), self.aggregated_table)

    def test_aggregate_partial_value_matrix(self):
        self.assertEqual(
            self.aggregator(self.partial_values),
            self.aggregated_partial_values,
        )


class SumTestCase(unittest.TestCase, AggregatorTestCase):
    def setUp(self):
        self.criteria = ["c01", "c02", "c03", "c04"]
        self.table = PerformanceTable(
            [[0, 1, 2, 3], [4, 5, 6, 7]],
            alternatives=["a01", "a02"],
            criteria=self.criteria,
        )
        self.values = self.table.alternatives_values["a01"]
        self.partial_values = PartialValueMatrix(
            [
                [Series([0, 1, 2, 3]), Series([4, 5, 6, 7])],
                [Series([4, 5, 6, 7]), Series([0, 1, 2, 3])],
            ],
            criteria=self.criteria,
        )
        self.aggregator = Sum()
        self.aggregated_values = 6
        self.aggregated_table = CommensurableValues(
            Series({"a01": 6, "a02": 22})
        )
        self.aggregated_partial_values = AdjacencyValueMatrix(
            [[6, 22], [22, 6]],
        )


class WeightedSumTestCase(unittest.TestCase, AggregatorTestCase):
    def setUp(self):
        self.criteria = ["c01", "c02", "c03", "c04"]
        self.table = PerformanceTable(
            [[0, 1, 2, 3], [4, 5, 6, 7]],
            alternatives=["a01", "a02"],
            criteria=self.criteria,
        )
        self.values = self.table.alternatives_values["a01"]
        self.partial_values = PartialValueMatrix(
            [
                [Series([0, 1, 2, 3]), Series([4, 5, 6, 7])],
                [Series([4, 5, 6, 7]), Series([0, 1, 2, 3])],
            ],
            criteria=self.criteria,
        )
        self.aggregator = WeightedSum({"c01": 1, "c02": 2, "c03": 1, "c04": 4})
        self.aggregated_values = 16
        self.aggregated_table = CommensurableValues(
            Series({"a01": 16, "a02": 48})
        )
        self.aggregated_partial_values = AdjacencyValueMatrix(
            [[16, 48], [48, 16]],
        )

    def test_weight_functions(self):
        expected = PerformanceTable(
            [[0, 2, 2, 12], [4, 10, 6, 28]],
            alternatives=["a01", "a02"],
            criteria=self.criteria,
        )
        self.assertEqual(
            self.aggregator.weight_functions(self.table), expected
        )

    def test_contructor_errors(self):
        with self.assertRaises(TypeError):
            WeightedSum(self.aggregator.criteria_weights, NominalScale(["a"]))
        with self.assertRaises(TypeError):
            WeightedSum(
                self.aggregator.criteria_weights, {0: NominalScale(["a"])}
            )
        with self.assertRaises(TypeError):
            WeightedSum(
                self.aggregator.criteria_weights, out_scale=NominalScale(["a"])
            )


class ChoquetIntegralTestCase(unittest.TestCase, AggregatorTestCase):
    def setUp(self):
        # Value taken from R kappalab package (7 digits accuracy)
        self.capacity = SetFunction(
            [
                0.0,
                0.07692307692307693,
                0.15384615384615385,
                0.38461538461538464,
                0.23076923076923078,
                0.46153846153846156,
                0.6153846153846154,
                0.8461538461538461,
                0.3076923076923077,
                0.5384615384615384,
                0.6923076923076923,
                0.9230769230769231,
                0.7692307692307693,
                1.0,
                1,
                1,
            ]
        )
        self.aggregator = ChoquetIntegral(self.capacity)
        self.values = Values([0.1, 0.9, 0.3, 0.8])
        self.table = PerformanceTable([[0.1, 0.9, 0.3, 0.8]])
        self.partial_values = PartialValueMatrix([[self.values.data]])
        self.aggregated_values = 0.6615385
        self.aggregated_table = CommensurableValues([self.aggregated_values])
        self.aggregated_partial_values = AdjacencyValueMatrix(
            [[self.aggregated_values]]
        )

    def test_constructor_errors(self):
        with self.assertRaises(ValueError):
            ChoquetIntegral(SetFunction([1, 0, 0, 1]))

    def test_aggregate_series(self):
        self.assertAlmostEqual(
            self.aggregator(self.values.data), self.aggregated_values
        )

    def test_aggregate_values(self):
        self.assertAlmostEqual(
            self.aggregator(self.values),
            self.aggregated_values,
        )

    def test_aggregate_dataframe(self):
        assert_series_equal(
            self.aggregator(self.table.data), self.aggregated_table.data
        )

    def test_aggregate_value_matrix(self):
        assert_series_equal(
            self.aggregator(self.table).data, self.aggregated_table.data
        )

    def test_aggregate_partial_value_matrix(self):
        assert_frame_equal(
            self.aggregator(self.partial_values).data,
            self.aggregated_partial_values.data,
        )


class ChoquetIntegralMobiusTestCase(ChoquetIntegralTestCase):
    def setUp(self):
        # Value taken from R kappalab package (7 digits accuracy)
        self.mobius = Mobius(
            [
                0,
                0.07692308,
                0.15384615,
                0.15384615,
                0.23076923,
                0.15384615,
                0.23076923,
                -0.15384615,
                0.30769231,
                0.15384615,
                0.23076923,
                -0.15384615,
                0.23076923,
                -0.15384615,
                -0.38461538,
                -0.07692308,
            ]
        )
        super().setUp()
        self.aggregator = ChoquetIntegral(self.mobius)

    def test_constructor_errors(self):
        with self.assertRaises(ValueError):
            ChoquetIntegral(SetFunction([1, 0, 0, 1]).mobius)


class OWATestCase(unittest.TestCase, AggregatorTestCase):
    def setUp(self):
        self.aggregator = OWA([0.2, 0.3, 0.1, 0.4])
        self.values = Values([0.6, 1.0, 0.3, 0.5])
        self.table = PerformanceTable([self.values.data.tolist()])
        self.partial_values = PartialValueMatrix([[self.values.data]])
        self.aggregated_values = 0.55
        self.aggregated_table = CommensurableValues([self.aggregated_values])
        self.aggregated_partial_values = AdjacencyValueMatrix(
            [[self.aggregated_values]]
        )

    def test_orness(self):
        self.assertEqual(OWA([1, 0, 0, 0, 0]).orness, 1)
        self.assertEqual(OWA([0, 0, 0, 1]).orness, 0)
        self.assertEqual(OWA([1 / 4, 1 / 4, 1 / 4, 1 / 4]).orness, 0.5)

    def test_andness(self):
        self.assertEqual(OWA([1, 0, 0, 0, 0]).andness, 0)
        self.assertEqual(OWA([0, 0, 0, 1]).andness, 1)
        self.assertEqual(OWA([1 / 4, 1 / 4, 1 / 4, 1 / 4]).andness, 0.5)

    def test_dispersion(self):
        self.assertEqual(OWA([0, 1, 0, 0]).dispersion, 0)
        self.assertEqual(OWA([1 / 4, 1 / 4, 1 / 4, 1 / 4]).dispersion, log(4))

    def test_divergence(self):
        self.assertEqual(OWA([1 / 2, 0, 0, 1 / 2]).divergence, 0.25)
        self.assertEqual(OWA([0, 0, 1 / 2, 0, 1 / 2, 0, 0]).divergence, 1 / 36)

    def test_balance(self):
        self.assertEqual(OWA([1, 0, 0, 0, 0]).balance, 1)
        self.assertEqual(OWA([0, 0, 0, 1]).balance, -1)
        self.assertEqual(OWA([1 / 4, 1 / 4, 1 / 4, 1 / 4]).balance, 0)

    def test_quantifier(self):
        self.assertEqual(OWA([0, 0, 1]).quantifier, [0, 0, 0, 1])
        self.assertEqual(OWA([1, 0, 0]).quantifier, [0, 1, 1, 1])

    def test_from_quantifier(self):
        self.assertEqual(OWA.from_quantifier([0, 1, 1]).weights, [1, 0])
        self.assertEqual(OWA.from_quantifier([0, 1, 1]).quantifier, [0, 1, 1])

    def test_and_aggregator(self):
        self.assertEqual(OWA.and_aggregator(5).weights, [0, 0, 0, 0, 1])

    def test_or_aggregator(self):
        self.assertEqual(OWA.or_aggregator(3).weights, [1, 0, 0])


class TestUlowa(unittest.TestCase, AggregatorTestCase):
    def setUp(self):
        self.fuzzy_sets = [
            FuzzyNumber([0, 0, 0, 0]),
            FuzzyNumber([0.0, 0.0, 0.0, 2.0]),
            FuzzyNumber([0.0, 2.0, 2.0, 5.0]),
            FuzzyNumber([2.0, 5.0, 5.0, 6.0]),
            FuzzyNumber([5.0, 6.0, 6.0, 7.0]),
            FuzzyNumber([6.0, 7.0, 8.0, 9.0]),
            FuzzyNumber([8.0, 9.0, 9.0, 10.0]),
            FuzzyNumber([9.0, 10.0, 10.0, 10.0]),
            FuzzyNumber([10, 10, 10, 10]),
        ]
        self.labels = ["N", "VL", "L", "M", "AH", "H", "VH", "P", "PP"]
        self.scale = FuzzyScale(Series(self.fuzzy_sets, index=self.labels))
        self.aggregator = ULOWA([0.0, 0.0, 0.5, 0.5, 0.0], self.scale)
        self.table = PerformanceTable(
            [
                ["VL", "VL", "P", "H", "VL"],
                ["VL", "VL", "H", "P", "P"],
                ["VL", "VL", "L", "M", "L"],
                ["VH", "L", "H", "H", "AH"],
                ["P", "L", "H", "L", "AH"],
                ["P", "VL", "VL", "M", "AH"],
            ],
            scales=self.scale,
        )
        self.values = self.table.alternatives_values[0]
        self.partial_values = PartialValueMatrix([[self.values.data]])
        self.aggregated_table = CommensurableValues(
            Series(["VL", "M", "VL", "AH", "M", "L"]), scale=self.scale
        )
        self.aggregated_values = self.aggregated_table.data[0]
        self.aggregated_partial_values = AdjacencyValueMatrix(
            [[self.aggregated_values]], scale=self.scale
        )

    def test_delta(self):
        self.assertEqual(self.aggregator.delta("N", "PP", 0.5), 5.0)

    def test_most_similar(self):
        self.assertEqual(
            self.aggregator.most_similar(
                "VL", "H", FuzzyNumber([5.5, 6.1, 6.5, 7.5])
            ),
            "AH",
        )
        self.assertEqual(
            self.aggregator.most_similar(
                "VL", "M", FuzzyNumber([5.5, 6.1, 6.5, 7.5])
            ),
            "M",
        )

    def test_aggregate_series(self):
        self.assertRaises(ValueError, self.aggregator, Series([]))
        return super().test_aggregate_series()
