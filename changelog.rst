Changelog
=========

v1.*
----

v1.0.0
~~~~~~

- stabilize API
- rename abstract method Sorter.clusterize to Sorter.sort
- add user API module-level doc-strings
- ensure compatibility with pandas<2.1 while complying with pandas>=2.1

v0.*
----

v0.7.0
~~~~~~

- split implementation from API
    - move all source code into mcda.internal subpackage
    - add API package hierarchy in mcda package
- use user API classes/functions in unit tests
- make graphviz/matplotlib dependencies optional
- complete unit tests for whole package except mcda.plot
- hide all 'apply' methods in callables
- mcda.core.functions
    - replace Interval.is_empty method by a property
    - replace PieceWiseFunction.continuous method by a property
- mcda.core.matrices
    - raise KeyError instead of ValueError when IAdjacencyMatrix has different set of labels for rows and columns
    - move to_graph/plot/save_plot methods from IAdjacencyMatrix to AdjacencyValueMatrix
    - raise KeyError if criteria are not unique when building a PartialValueMatrix
    - use new view types for *_values properties to replace dictionaries
- mcda.core.relations
    - hide all classes _validate method
    - make Relation.types method static
    - replace PreferenceStructure get/del methods with multiple accessors and views
- mcda.core.scales
    - make QuantitativeScale.normal method static
    - make DiscreteQuantitativeScale.binary method static
    - replace NormalScale/BinaryScale classes with type aliases
- mcda.core.set_functions
    - hide ISetFunction.validate method
    - add argument to enable/disable ISetFunction object validation on construction (enabled by default)
    - make SetFunction.uniform_capacity method static
- mcda.core.values
    - add new class Value
    - make Values a mappable of Value

v0.6.1
~~~~~~

- add AdditivePerformanceTable class which adds `aggregated_scale` attribute to row-wise sums
- add the Sum aggregator
- AdditiveValueFunctions changes
    - make it a subclass of CriteriaFunctions
    - add an aggregate method which sums values
    - return an AdditivePerformanceTable object when passed on a PerformanceTable
- inverse order of Electre3/ElectreTri constructor arguments preference and indifference thresholds

v0.6.0
~~~~~~

- add support for python 3.12
- remove python 3.7 from supported versions
- rework completely API of whole package (only most notable changes listed below)
    - change organizations of scales
    - reorganize main data structures
    - add stype (in_stype/out_stype) attribute to all classes manipulating data
    - change callable classes so typing can be tracked while using their __call__ method (and hide all input type specific methods)
    - add aliases for PreferenceDirection values
    - replace OutrankingMatrix/Ranking classes with type aliases
    - move data transform/normalize methods to mcda.core.transformers module
    - integrate performance table as an attribute of all MCDA algotithms
- change discordance method in electre so it returns partial values
    - change veto thresholds so they are defined per criterion
- add cell property to access IMatrix cells directly
- speed up PreferenceStructure to/from outranking matrix conversion methods

v0.5.1
~~~~~~

- remove python 3.6 from supported versions
- make internal classes private
- core
    - matrices
        - remove all block matrix classes
        - forbid duplicated index/column labels
    - performance_tables
        - add concat class method to PerformanceTable
        - replace Container type with simple dict in alternative/criteria values
    - values
        - forbid duplicated index labels
        - remove Container class and replace its usage with simple dict
- outranking
    - transform profiles to performance table scales when applying ElectreTri and SRMP (bugfix)

v0.5.0
~~~~~~

- document inherited members
- rework Documentation
- add notebooks to documentation
- core
    - values:
        - add strong and normal dominance methods to ScaleValues
        - add generic Container class
        - rename transform_to method of ScaleValues to transform
    - performance_table
        - add efficients property to PerformanceTable
        - merge apply_ methods in one methos apply
        - replace get_ methods by [alternatives/criteria]_values container views
    - matrices
        - add classes for block matrix and profile Vs alternatives matrix
        - add combinations of classes in matrix module
    - make all __repr__ methods uncovered by unit tests
    - add categories module
    - add criteria_functions module
- outranking
    - fix wrong behaviour of ElectreTri class
    - change type of concordance, discordance and credibility matrices in Electre
    - reorganize electre class hierarchy
    - reorganize promethee module with a more modular structure

v0.4.1
~~~~~~

- outranking: fix bug in ElectreTri which failed when any other set of criteria than 0..N was used

v0.4.0
~~~~~~

- replace API by an object oriented one in whole package
- add preference elicitation algorithm for SRMP


v0.3.6
~~~~~~

- fix SRMP progressive ranking plot function


v0.3.5
~~~~~~

- replace all criteria/alternatives values by labelled dict, pandas Series or DataFrames
- move all matrix related functions needed by mcda.core.relations into it
- core
    - replace list of lists by pandas dataframe for performance table
    - split sorting module in relations module
    - relations: add basic relations functions
- outranking
    - add SRMP algorithm
    - remove custom graph data structure
- plot
    - change plot_relation so it can plot any relations without any supposition
    - add area plot, parallel coordinates plot, stacked bar plot
    - add text and annotations
    - add horizontal and vertical stripes
    - add legend
    - add color and alpha parameters
    - add linestyle and marker style parameters for line plot


v0.3.4
~~~~~~

- core
    - rework fuzzy scales and numbers (trapezoidal shape only)
- mavt
    - add OWA divergence and balance measures
    - add ULOWA aggregator
- outranking
    - add promethee1 algorithm
    - add Promethee 2 algorithm
    - add Electre 2 algorithm
    - add Electre 3 algorithm
    - add Electre TRI Algorithm
    - add utils.py for management functions
- plot
    - plot promethee GAIA
    - plot method for graph using matrix or relations_list
    - matrix plot can select displayed values


v0.3.3
~~~~~~

- mavt
    - add OWA aggregator and related functions
- outranking
    - add Electre 1 algorithm
    - plot method for graph


v0.3.2
~~~~~~

- mavt
    - show all aggregators functions in the API


v0.3.1
~~~~~~

- plot
    - add rlimits attribute for RadarPlot


v0.3.0
~~~~~~

- core
    - add sets and set_functions modules
- mavt
    - add choquet integral aggregators
- set public API for all modules (__all__ module constants)
- change function names
    - core.performance_table.valid_alternatives -> in_scales_alternatives
    - mavt.aggregators.weighted_sum_after_normalization -> normalized_weighted_sum
    - mavt.uta.generate_g_matrix -> generate_criteria_values_matrix
    - mavt.uta.generate_u_variables -> generate_marginal_utility_variables
    - mavt.uta.generate_sigma_variables -> generate_alternatives_errors_variables
    - mavt.uta.compute_uta_after_normalization -> normalized_uta
    - mavt.uta.compute_uta_star_after_normalization -> normalized_uta_star
    - mavt.uta.compute_post_optimality -> post_optimality
    - mavt.uta.compute_star_post_optimality -> star_post_optimality
    - mavt.uta.compute_uta -> uta
    - mavt.uta.compute_uta_star -> uta_star
- remove Matrix type alias and replace all references with List[List[]]
- rework completely plotting utilities in subpackage plot


v0.2.1
~~~~~~

- fix mypy errors when importing this package

v0.2.0
~~~~~~

- add py.typed file to packages files for allowing type-checking on import
- mavt
    - add weighted sum aggregator
    - add UTA & UTA* algorithm
- core.performance_table
    - add function to find alternatives with performances in scales
    - add subtable function
- core.sorting
    - add relation type enum
- generalize transform & normalize & range methods
- add functions with an input scale that can be transformed
- rename nominative scale to nominal scale


v0.1.0
~~~~~~
(start of the changelog)
