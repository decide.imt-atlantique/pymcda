"""This module gathers structure used to define categories.
"""
from .internal.core.categories import (
    BoundedCategoryProfile,
    CentralCategoryProfile,
)

__all__ = [
    "BoundedCategoryProfile",
    "CentralCategoryProfile",
]
