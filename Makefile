# --
SHELL := bash
.ONESHELL:
.SHELLFLAGS := -o errexit -o nounset -o pipefail -c
.DELETE_ON_ERROR:
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

# --
ISORT_OPTS=--profile black --combine-star --use-parentheses -m VERTICAL_HANGING_INDENT -w 79
FLAKE_OPTS=--max-line-length=79 --ignore=E203,W503
SRCS:=src tests

# --
.PHONY: install build deploy deploy-test

install:
	pip install -r requirements.txt

build:
	python -m build

deploy-test: build
	python -m twine upload --repository testpypi dist/*

deploy: build
	python -m twine upload dist/*

# --
.PHONY: isort-check isort-apply flake8 black-check black-apply mypy lint lint-apply doc api doc-copy

isort-check:
	isort ${ISORT_OPTS} --check --diff ${SRCS}

isort-apply:
	isort --atomic ${ISORT_OPTS} ${SRCS}

flake8:
	flake8 ${FLAKE_OPTS} ${SRCS}

black-check:
	black --line-length 79 --check ${SRCS}

black-apply:
	black --line-length 79 ${SRCS}

mypy:
	mypy --ignore-missing-imports ${SRCS}

lint: flake8 isort-check black-check mypy

lint-apply: isort-apply black-apply

api:
	cd doc
	sphinx-apidoc -fMeT -d 1 -o ./api/ -t=./templates ../src

doc-copy:
	cp changelog.rst doc/.

doc: api doc-copy
	cd doc
	$(MAKE) html

# --
.PHONY: test tox clean coverage coverage-report coverage-report-html cov-html notebook-test back-test

test:
	python -m pytest

tox:
	tox

coverage:
	coverage run --branch -m pytest

coverage-check: coverage
	coverage report --fail-under=100

coverage-report: coverage
	coverage report

coverage-report-html cov-html: coverage
	coverage html

notebook-test:
	cd doc/notebooks
	jupyter nbconvert --to notebook --execute examples/*.ipynb --output-dir tmp
	jupyter nbconvert --to notebook --execute tutorials/*.ipynb --output-dir tmp

back-test:
	. ./scripts/version.sh
	./scripts/back-test.sh ${MAJOR}

clean:
	-find . -type d -name '__pycache__' -exec rm --recursive --force {} +
	rm -Rf doc/notebooks/tmp
	coverage erase
	rm -Rf htmlcov
	rm -Rf doc/html doc/doctrees doc/api doc/changelog.rst
